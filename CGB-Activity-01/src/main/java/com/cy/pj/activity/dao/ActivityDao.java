package com.cy.pj.activity.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.pj.activity.pojo.Activity;

@Mapper
public interface ActivityDao {
	/**
	 * 	修改活动信息状态,将其设置为已结束状态0;
	 * @param id
	 * @return
	 */
	@Update("update tb_activity set state=0 where id=#{id}")
	int updateState(Integer id);
	/**
	 * 	添加活动信息
	 * @param entity
	 * @return
	 */
	int insertObject(Activity entity);
	 /**
	  *	 获取所有活动信息,一行记录映射为一个Activity对象(row map-行映射)
	  * @return
	  */
	 @Select("SELECT * FROM tb_activity order by createdTime desc")
	 List<Activity> findActivitys();
}







