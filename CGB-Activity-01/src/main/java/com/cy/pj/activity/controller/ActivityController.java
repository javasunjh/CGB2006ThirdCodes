package com.cy.pj.activity.controller;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.cy.pj.activity.pojo.Activity;
import com.cy.pj.activity.service.ActivityService;

@Controller
@RequestMapping("/activity/")
public class ActivityController {

	 @Autowired
	 private ActivityService activityService;
	 /**
	  * 	基于此方法返回activity页面
	  * @return
	  */
	 @RequestMapping("doActivityUI")
	 public String doActivityUI() {
		 return "activity";
	 }
	 
	 @RequestMapping("doSaveObject")
	 @ResponseBody
	 public String doSaveObject(Activity entity) {
		 System.out.println(entity);//假如希望这样检查对象内容,则需要重写对象的toString方法
		 activityService.saveObject(entity);
		 //return  "activity";//这样返回，刷新的是整个页面
		 System.out.println("ABCD");
		 return "save ok";
	 }
	 
	 @RequestMapping("doFindActivitys")
	 @ResponseBody //告诉spring 框架将返回值对象转换为json格式字符串
	 public List<Activity> doFindActivitys()throws Exception{
		 // Thread.sleep(5000);
		 List<Activity> list=activityService.findActivitys();
		 return list;
	 }
 
}
