package com.cy.java.tryf;

public class TestTryCatchFinally {

	static int doMethod(int a,int b) {
		int result=0;
		try {
		   result=a/b;
		   return result;//2
		}catch(Exception e) {
		   System.out.println(e.getMessage());
		   throw e;
		}finally {
		   result=100;
		   System.out.println("finally");
		}
	}
	public static void main(String[] args) {
		int result=doMethod(20,10);
		System.out.println("result="+result);
	}
}
