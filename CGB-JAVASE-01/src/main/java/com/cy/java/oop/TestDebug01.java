package com.cy.java.oop;

public class TestDebug01 {

	static void doMethod01() {
		System.out.println("doMethod01");
        doMethod02();
	}
	static void doMethod02() {
		System.out.println("doMethod02");
		doMethod03();
	}
	static void doMethod03() {
		System.out.println("doMethod03");
	}
	public static void main(String[] args) {
		System.out.println("main");
		doMethod01();
	}
}
