package com.cy.pj.goods.service;

import java.util.List;

import com.cy.pj.goods.pojo.Goods;

/**
  * 商品模块的业务层接口，负责具体业务标准的定义
 * @author pc
 */
public interface GoodsService {
	  Goods findById(Integer id);
	  int saveGoods(Goods goods);
	  int updateGoods(Goods goods);
	  int deleteById(Integer id);
	  List<Goods> findGoods();
}
