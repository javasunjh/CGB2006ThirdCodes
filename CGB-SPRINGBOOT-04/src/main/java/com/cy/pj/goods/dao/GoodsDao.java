package com.cy.pj.goods.dao;
import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.cy.pj.goods.pojo.*;
/**
 * @Mapper 用于描述(做标记)数据层访问接口,用于告诉mybatis框架,
  *  使用此注解描述的接口要由底层为创建实现类.在实现类中基于mybatis
 * API实现与数据库的交互.这个类的对象最后会交给spring管理.
 * FAQ?
  * 我们添加了MyBatis-Spring-Boot-Starter这个依赖以后,此依赖内部做了什么?
 */
@Mapper
public interface GoodsDao {//还有一些企业这个GoodsDao的名字会定义为GoodsMapper
	 
	  @Insert("insert into tb_goods(name,remark,createdTime) values (#{name},#{remark},#{createdTime})")
	  int insertGoods(Goods goods);
	  
	  @Update("update tb_goods set name=#{name},remark=#{remark} where id=#{id}")
	  int updateGoods(Goods goods);
	  
	  /**基于商品id查询商品信息*/
	  @Select("select * from tb_goods where id=#{id}")
	  Goods findById(Integer id);
	
	  @Select("select * from tb_goods")
	  List<Goods> findGoods();
	
	  /**
	   * 	基于id执行商品信息的删除,在mybatis中假如SQL映射语句比较简单\
	   * 	可以直接在dao方法上以注解方式进行定义.
	   * @param id 商品id
	   * @return 删除的行数
	   */
	  @Delete("delete from tb_goods where id=#{id}")
	  int deleteById(Integer id);
	  
	  /**
	   * 	基于多个id执行商品删除业务
	   * @param ids 可变参数,用于接收传入的商品id值
	   * @return 删除行数
	   */
	  int deleteObjects(@Param("ids")Integer...ids);
}



