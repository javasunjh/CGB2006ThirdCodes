package com.cy.pj.common.cache;

import org.springframework.stereotype.Component;

@Component //假如省略名字，则bean的名字默认为类名，然后首字母小写。
//@Component("softCache")//@Component 注解描述bean时给bean起名字为softCache
public class SoftCache implements Cache {//map.put("softCache",object instance)

}
