package com.cy.pj.common.service;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import com.cy.pj.common.cache.Cache;
/**
 * @Component 描述类时,用于告诉spring框架,由spring构建此类型对象.
 * FAQ?
 * 1)Spring框架如何为我们的类型构建对象?通过反射
 * 2)反射构建对象需要用到的API是什么?(Class,Constructor)
 * 
 * 
 */
@Component
//@Component("searchService")
@Scope("singleton")
public class SearchService {
	//1.@Autowired 描述属性
     //@Autowired
	 private Cache cache;
	 
//	 public SearchService() {
//		System.out.println("SearchService()");
//	 }
	 
	//2.@Autowired 描述set方法(通常配合无参数构造函数使用)
//	 @Autowired
//	 public void setCache(  @Qualifier("softCache")Cache cache) {
//		 System.out.println("setCache()");
//		this.cache = cache;
//	 }
	 
	 //3.@Autowired 描述构造方法
	 @Autowired //描述构造方法时这个@Autowired注解可以省略
	 public SearchService(@Qualifier("softCache") Cache cache) {
		 System.out.println("SearchService( Cache cache) ");
		 this.cache=cache;
	 }
	 
	 /**外界通过此方法获取由spring容器为SearchService注入的cache对象*/
	 public Cache getCache() {
		return cache;
	 }
}
