package com.cy.pj.common.pool;
import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;

import org.springframework.context.annotation.Lazy;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
/**
 * FAQ?
 * 1)假如将此类交给spring管理，请问默认spring何时创建此对象？你怎么知道的？
 * 2)假如一个对象创建以后，存储到内存，长时间不用会有什么影响？
 * 3)你知道Spring中，Bean对象的延迟加载特性吗？(了解)
 * 4)你知道什么场景下这个对象适合延迟加载吗？大对象，稀少用。
 * 5)对象创建以后，应用结束要销毁对象吗？
 * 6)程序中的每个对象都有生命周期(正确)，不见得每个对象都有生命周期方法。
 */
@Scope("prototype") //每次从spring容器获取对象都会创建一个新的实例
//@Scope("singleton")//默认就是单例作用域(这个类的实例在一个spring容器中默认只有一份)
//@Lazy(false)//默认为true
//@Lazy(true)//此注解用于告诉spring框架，它描述的类的实例，假如暂时用不到，就不要先创建。
//@Lazy(value=true)
@Component
public class ObjectPool {
    /**构建对象时会执行对象的构造函数*/
	public ObjectPool() {
	   System.out.println("ObjectPool()");
	}
	/**@PostConstruct 注解描述的方法会在对象构建以后执行，用于执行一些初始化操作*/
	@PostConstruct 
	public void init() {
		System.out.println("init()");
	}
	/**@PreDestroy 注解描述的方法会在单例对象销毁之前执行，spring容器在销毁之前
	 * 会先将容器(Bean池)中的对象进行移除，在移除对象时，假如对象中定义了生命周期销
	 * 毁方法，此时还会调用对象的生命周期销毁方法(在这样的方法中可以做一些资源释放操
	 * 作)。
	 * */
	@PreDestroy
	public void close() {
		System.out.println("close()");
	}
}
